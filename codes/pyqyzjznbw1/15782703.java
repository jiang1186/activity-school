/**  
 * 冒泡排序函数  
 * 依次比较相邻的两个数，将较大的数移到后面，较小的数移到前面  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) { // 外层循环，控制所有元素遍历的次数  
        for (int j = 0; j < n - i - 1; j++) { // 内层循环，从前往后依次比较相邻的两个元素  
            if (a[j] > a[j + 1]) { // 如果前面的数比后面的数大，则交换它们的位置  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //enD

/**  
 * 冒泡排序函数  
 * 该函数使用冒泡排序算法对数组进行升序排序  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) {  
        // 标记是否发生交换，用于优化，如果在内层循环中没有发生交换，说明已经有序  
        boolean swapped = false;  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果前一个元素大于后一个元素，则交换它们  
            if (a[j] > a[j + 1]) {  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 发生了交换  
                swapped = true;  
            }  
        }  
        // 如果在内层循环中没有发生交换，说明已经有序，可以提前结束排序  
        if (!swapped) {  
            break;  
        }  
    }  
} //end
